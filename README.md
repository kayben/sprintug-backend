This README provides an overview of the backend for your restaurant management application. It includes information about the project structure, installation, available routes, and additional details.

## Project Structure

The backend is structured as follows:

- `app.js`: The entry point of the application that sets up the server and middleware.
- `models/`: Contains the Mongoose model for interacting with the database.
- `controllers/`: Contains the controller methods for handling CRUD operations.
- `tests/`: Contains the test file for the backend using Chai and Mocha.
- `config/`: Contains the database configuration files.
- `util/`: Contains utility functions or helper modules.

## Installation

To run the backend locally, follow these steps:

1. Clone the repository:

```
git clone <repository-url>
```

2. Install the dependencies:

```
cd restuarant-backend
npm install
```

3. Set up the configuration:

   -Run node app.js


The backend server will start running on port 4500.

## Available Routes

The backend provides the following routes for managing restaurants:

- `GET /api/restaurants`: Fetches all restaurants.
- `POST /api/create-restaurant`: Creates a new restaurant.
- `PUT /api/update-restaurant/:id`: Updates an existing restaurant.
- `DELETE /api/delete-restaurant/:id`: Deletes a restaurant.

## Testing

The backend includes test cases using Chai and Mocha to ensure the functionality of the API endpoints. To run the tests, use the following command:

```
npm test
```

The tests cover the controller methods for creating, retrieving, updating, and deleting restaurants.

