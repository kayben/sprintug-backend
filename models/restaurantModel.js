const Joi = require('joi');
const { Schema, model } = require('mongoose');

const restaurantSchema = new Schema({
  rid: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true,
    // validate: {
    //   validator: (value) => {
    //     const schema = Joi.string().min(2).max(50).required();
    //     return schema.validate(value).error === null;
    //   },
    //   message: 'Name must be a string of 2 to 50 characters'
    // }
  },
  cuisine: {
    type: String,
    required: true,
    // validate: {
    //   validator: (value) => {
    //     const schema = Joi.string().min(2).max(50).required();
    //     return schema.validate(value).error === null;
    //   },
    //   message: 'Cuisine must be a string of 2 to 50 characters'
    // }
  },
  location: {
    type: String,
    required: true,
    // validate: {
    //   validator: (value) => {
    //     const schema = Joi.string().min(2).max(50).required();
    //     return schema.validate(value).error === null;
    //   },
    //   message: 'Location must be a string of 2 to 50 characters'
    // }
  },
  description: {
    type: String,
    required: true,
    // validate: {
    //   validator: (value) => {
    //     const schema = Joi.string().min(10).max(500).required();
    //     return schema.validate(value).error === null;
    //   },
    //   message: 'Description must be a string of 10 to 500 characters'
    // }
  },
  imageUrl: {
    type: String,
    required: true,
    // validate: {
    //   validator: (value) => {
    //     const schema = Joi.string().uri().required();
    //     return schema.validate(value).error === null;
    //   },
    //   message: 'ImageUrl must be a valid URL'
    // }
  }
});

const Restaurant = model('Restaurant', restaurantSchema);

module.exports = Restaurant;
