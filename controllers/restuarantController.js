const Restaurant = require('../models/restaurantModel');
const generateRestaurantId = () => {
  const randomString = Math.random().toString(36).substr(2, 9);
  return `RST-${randomString}`;
};


const restaurantController = {
  // Create a new restaurant
  createRestaurant: async (req, res) => {
    try {
      const { name, cuisine, location, description, imageUrl } = req.body;

      const rid = generateRestaurantId();
      const restaurant = await Restaurant.create({ rid, name, cuisine, location, description, imageUrl });
      const response = {
        status: 'success',
        message: 'Restaurant created successfully',
        data: restaurant
      }
      res.status(201).json(response);
    } catch (error) {
      console.log(error);
      const response = {
        status: 'failed',
        message: 'Unable to create restaurant',
        data: {}
      }
      res.status(500).json(response);
    }
  },

  // Get all restaurants
  getAllRestaurants: async (res) => {
    try {
      const restaurants = await Restaurant.find();
      const response = {
        status: 'success',
        message: 'Restaurants retrieved successfully',
        data: restaurants
      }
      res.status(200).json(response);
    } catch (error) {
      const response = {
        status: 'failed',
        message: 'Unable to retrieve restaurants',
        data: {}
      }
      res.status(500).json(response);
    }
  },

  // Update a restaurant
  updateRestaurant: async (req, res) => {
    try {
      const { id } = req.params;
      const { name, cuisine, location, description, imageUrl } = req.body;
      const restaurant = await Restaurant.findOneAndUpdate(
        { rid: id }, 
        { name, cuisine, location, description, imageUrl },
        { new: true }
      );
      if (!restaurant) {
        return res.status(404).json({ error: 'Restaurant not found' });
      }
      const response = {
        status: 'success',
        message: 'Restaurant updated successfully',
        data: restaurant
      }
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      const response = {
        status: 'failed',
        message: 'Unable to update restaurant',
        data: {}
      }
      res.status(500).json(response);
    }
  },

  // Delete a restaurant
  deleteRestaurant: async (req, res) => {
    try {
      const { id } = req.params;
      const restaurant = await Restaurant.findOneAndDelete({ rid: id });
      if (!restaurant) {
        return res.status(404).json({ error: 'Restaurant not found' });
      }
      const response = {
        status: 'success',
        message: 'Restaurant deleted successfully',
        data: {}
      }
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      const response = {
        status: 'failed',
        message: 'Unable to delete restaurant',
        data: {}
      }
      res.status(500).json(response);
    }
  }
};

module.exports = restaurantController;
