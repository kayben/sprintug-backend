const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const app = require('../app');
const Restaurant = require('../models/restaurantModel');

chai.use(chaiHttp);

describe('Restaurant Controller', () => {
    let restaurantId;

    before(async () => {
        await Restaurant.deleteMany({});
    });

    describe('createRestaurant', () => {
        it('should create a new restaurant', async () => {
            const newRestaurant = {
                rid: 'RST-111',
                name: 'Restaurant A',
                cuisine: 'Italian',
                location: 'New York',
                description: 'Lorem ipsum dolor sit amet',
                imageUrl: 'restaurant_a.jpg',
            };

            const res = await chai
                .request(app)
                .post('/api/restaurants')
                .send(newRestaurant);

            expect(res).to.have.status(201);
            expect(res.body.status).to.equal('success');
            expect(res.body.message).to.equal('Restaurant created successfully');
            expect(res.body.data).to.be.an('object');
            expect(res.body.data.name).to.equal(newRestaurant.name);
            expect(res.body.data.cuisine).to.equal(newRestaurant.cuisine);
            expect(res.body.data.location).to.equal(newRestaurant.location);
            expect(res.body.data.description).to.equal(newRestaurant.description);
            expect(res.body.data.imageUrl).to.equal(newRestaurant.imageUrl);

            restaurantId = res.body.data.rid;
        });
    });

    describe('getAllRestaurants', () => {
        it('should retrieve all restaurants', async () => {
            const res = await chai.request(app).get('/api/restaurants');

            expect(res).to.have.status(200);
            expect(res.body.status).to.equal('success');
            expect(res.body.message).to.equal('Restaurants retrieved successfully');
            expect(res.body.data).to.be.an('array');
            expect(res.body.data.length).to.equal(1);
        });
    });

    describe('updateRestaurant', () => {
        it('should update a restaurant', async () => {
            const updatedRestaurant = {
                name: 'Updated Restaurant',
                cuisine: 'Mexican',
                location: 'Los Angeles',
                description: 'Updated description',
                imageUrl: 'updated_restaurant.jpg',
            };

            const res = await chai
                .request(app)
                .put(`/api/restaurants/${restaurantId}`)
                .send(updatedRestaurant);

            expect(res).to.have.status(200);
            expect(res.body.status).to.equal('success');
            expect(res.body.message).to.equal('Restaurant updated successfully');
            expect(res.body.data).to.be.an('object');
            expect(res.body.data.name).to.equal(updatedRestaurant.name);
            expect(res.body.data.cuisine).to.equal(updatedRestaurant.cuisine);
            expect(res.body.data.location).to.equal(updatedRestaurant.location);
            expect(res.body.data.description).to.equal(updatedRestaurant.description);
            expect(res.body.data.imageUrl).to.equal(updatedRestaurant.imageUrl);
        });

        it('should return an error if the restaurant is not found', async () => {
            const invalidRestaurantId = 'invalid_id';

            const res = await chai
                .request(app)
                .put(`/api/restaurants/${invalidRestaurantId}`)
                .send({ name: 'Updated Restaurant' });

            expect(res).to.have.status(404);
            expect(res.body.error).to.equal('Restaurant not found');
        });
    });

    describe('deleteRestaurant', () => {
        it('should delete a restaurant', async () => {
            const res = await chai.request(app).delete(`/api/restaurants/${restaurantId}`);

            expect(res).to.have.status(200);
            expect(res.body.status).to.equal('success');
            expect(res.body.message).to.equal('Restaurant deleted successfully');
        });

        it('should return an error if the restaurant is not found', async () => {
            const invalidRestaurantId = 'invalid_id';

            const res = await chai.request(app).delete(`/api/restaurants/${invalidRestaurantId}`);

            expect(res).to.have.status(404);
            expect(res.body.error).to.equal('Restaurant not found');
        });
    });
});
