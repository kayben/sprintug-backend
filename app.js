const express = require('express');
const connectToDB = require('./config/db');
const cors = require('cors');



const app = express();
const port = 4500;

const restaurantController = require('./controllers/restuarantController');


// Middleware to parse JSON data
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Connect to the database
connectToDB();

// Middleware to enable CORS
app.use(cors());

// Routes for CRUD operations on restaurants
app.post('/create-restaurant', restaurantController.createRestaurant);
app.put('/update-restaurant/:id', restaurantController.updateRestaurant);
app.delete('/delete-restaurant/:id', restaurantController.deleteRestaurant);
app.get('/restaurants', restaurantController.getAllRestaurants);

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
