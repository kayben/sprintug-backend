const generateRestaurantId = () => {
    const randomString = Math.random().toString(36).substr(2, 9);
    return `RST-${randomString}`;
  };
  
module.exports = generateRestaurantId;
  